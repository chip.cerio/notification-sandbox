package com.chipcerio.notificationprogress

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    
    private lateinit var button: Button
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.button)
        button.setOnClickListener { startBackgroundService() }
    }
    
    private fun startBackgroundService() {
        startService(Intent(this, MainService::class.java))
    }
    
    companion object {
        const val PRIMARY_CHANNEL_ID = "notification_channel"
        const val NOTIFICATION_ID = 1
        const val PROGRESS_MAX = 100
    }
}
