package com.chipcerio.notificationprogress

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.Color
import android.os.Build

class App : Application() {
    
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }
    
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                MainActivity.PRIMARY_CHANNEL_ID,
                "upload notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.apply {
                enableLights(false)
                lightColor = Color.RED
                enableVibration(false)
                description = "Response to question notification"
            }
            val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }
}