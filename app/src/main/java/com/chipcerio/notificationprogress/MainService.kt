package com.chipcerio.notificationprogress

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class MainService : Service() {
    
    private lateinit var builder: NotificationCompat.Builder
    private lateinit var notificationManager: NotificationManagerCompat
    private lateinit var runnable: Runnable
    private lateinit var handler: Handler
    private var count = 0
    
    private fun message(msg: String) {
        Log.d(TAG, msg)
    }
    
    private fun getNotificationBuilder(): NotificationCompat.Builder {
        return NotificationCompat.Builder(this, MainActivity.PRIMARY_CHANNEL_ID).apply {
            setSmallIcon(R.drawable.ic_vello_notification_default)
            setContentTitle("VID_20191125_0923.mp4")
            setContentText("Upload in progress")
            setProgress(MainActivity.PROGRESS_MAX, 0, true)
            setOngoing(true)
            setOnlyAlertOnce(true)
            priority = NotificationCompat.PRIORITY_HIGH
        }
    }
    
    private fun createRunnable(): Runnable {
        return object : Runnable {
            override fun run() {
                count++
                message("count: $count")
                if (count < 10) {
                    builder.apply {
                        val progress = count * 10
                        setContentText("$progress / 100 complete")
                        setProgress(MainActivity.PROGRESS_MAX, progress, true)
                    }
                    notificationManager.notify(MainActivity.NOTIFICATION_ID, builder.build())
                    handler.postDelayed(this, 1000)
                } else {
                    message("Completed")
                    builder.apply {
                        setContentText("Upload Complete")
                        setProgress(0, 0, false)
                        setOngoing(false)
                    }
                    notificationManager.notify(MainActivity.NOTIFICATION_ID, builder.build())
                }
            }
        }
    }
    
    override fun onCreate() {
        super.onCreate()
        notificationManager = NotificationManagerCompat.from(this)
        builder = getNotificationBuilder()
        runnable = createRunnable()
        handler = Handler(Looper.getMainLooper())
    }
    
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        notificationManager.notify(MainActivity.NOTIFICATION_ID, builder.build())
        handler.postDelayed(runnable, 1000)
        return START_STICKY
    }
    
    override fun onDestroy() {
        handler.removeCallbacks(runnable)
        count = 0
        builder = getNotificationBuilder()
        message("Service destroyed")
        super.onDestroy()
    }
    
    override fun onBind(intent: Intent?): IBinder? = null
    
    companion object {
        const val TAG = "MainService"
    }
}